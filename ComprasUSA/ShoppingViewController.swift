//
//  ViewController.swift
//  ComprasUSA
//
//  Created by Paulo Gutemberg on 04/09/19.
//  Copyright © 2019 Paulo Gutemberg. All rights reserved.
//

import UIKit

//NumberFormatter
//SizeClass
//Constrants

class ShoppingViewController: UIViewController {

	@IBOutlet weak var txtDolar: UITextField!
	@IBOutlet weak var lbRealDescricao: UILabel!
	@IBOutlet weak var lbReal: UILabel!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
	
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		setAmmount()
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		txtDolar.resignFirstResponder()
		setAmmount()
	}
	
	func setAmmount(){
		tc.valorCompra = tc.convertToDouble(txtDolar.text!)
		lbReal.text = tc.getFormattedValue(of:
			tc.valorCompra * tc.dolar, withCurrency: "R$ ")
		let dol = tc.getFormattedValue(of:
		tc.valorCompra * tc.dolar, withCurrency: "")
		
		lbRealDescricao.text = "Valor sem impostos (dólar \(dol))"
	}
}

