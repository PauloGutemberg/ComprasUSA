//
//  CalculadorTaxas.swift
//  ComprasUSA
//
//  Created by Paulo Gutemberg on 29/12/19.
//  Copyright © 2019 Paulo Gutemberg. All rights reserved.
//

import Foundation

class CalculadorTaxas {
	
	//singleton
	static let shared = CalculadorTaxas()
	var dolar: Double = 3.5
	var iof: Double = 6.38
	var taxaEstado: Double = 7.0
	var valorCompra: Double = 0.0
	
	let formatter = NumberFormatter()
	
	//variavel computada
	var valorCompraEmReal: Double{
		return valorCompra * dolar
	}
	
	var valorTaxaEstado: Double{
		return valorCompra * taxaEstado/100
	}
	
	var iofValue: Double {
		return (valorCompra + taxaEstado) * iof/100
	}
	
	//funcoes
	func calcular(usandoCartaoDeCredito: Bool) -> Double{
		var valorFinal = valorCompra + valorTaxaEstado
		if(usandoCartaoDeCredito){
			valorFinal += iofValue
		}
		return valorFinal * dolar
	}
	
	func convertToDouble(_ string: String) -> Double{
		formatter.numberStyle = .none
		return formatter.number(from: string)!.doubleValue
	}
	
	func getFormattedValue(of value: Double, withCurrency currency: String) -> String {
		formatter.numberStyle = .currency
		formatter.currencySymbol = currency
		formatter.alwaysShowsDecimalSeparator = true
		return formatter.string(for: value)!
	}
	
	private init(){
		formatter.usesGroupingSeparator = true
	}
	
	//add 
}
