//
//  SettingsViewController.swift
//  ComprasUSA
//
//  Created by Paulo Gutemberg on 09/09/19.
//  Copyright © 2019 Paulo Gutemberg. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

	@IBOutlet weak var txtDolar: UITextField!
	@IBOutlet weak var txtIof: UITextField!
	@IBOutlet weak var txtImpostoEstado: UITextField!
	
	override func viewDidLoad() {
        super.viewDidLoad()
		
		txtDolar.text = tc.getFormattedValue(of: tc.dolar, withCurrency: "")
		txtIof.text = tc.getFormattedValue(of: tc.iof, withCurrency: "")
		txtImpostoEstado.text = tc.getFormattedValue(of: tc.taxaEstado, withCurrency: "")
	}
    
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		view.endEditing(true)
	}
	
	func setValues(){
		tc.dolar = tc.convertToDouble(txtDolar.text!)
		tc.iof = tc.convertToDouble(txtIof.text!)
		tc.taxaEstado = tc.convertToDouble(txtImpostoEstado.text!)
	}
	
}

extension SettingsViewController: UITextFieldDelegate {
	
	func textFieldDidEndEditing(_ textField: UITextField) {
		setValues()
	}
}
