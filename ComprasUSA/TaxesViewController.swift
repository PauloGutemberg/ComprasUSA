//
//  TaxesViewController.swift
//  ComprasUSA
//
//  Created by Paulo Gutemberg on 09/09/19.
//  Copyright © 2019 Paulo Gutemberg. All rights reserved.
//

import UIKit

class TaxesViewController: UIViewController {

	@IBOutlet weak var lbDolar: UILabel!
	@IBOutlet weak var lbImpostoEstado: UILabel!
	@IBOutlet weak var lbIof: UILabel!
	@IBOutlet weak var lbIofDescricao: UILabel!
	@IBOutlet weak var swCartaoCredito: UISwitch!
	@IBOutlet weak var lbReal: UILabel!
	
	@IBOutlet weak var lbImpostoEstadoDescricao: UILabel!
	
	override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		calcularTaxas()
	}

	func calcularTaxas(){
		
		lbImpostoEstadoDescricao.text = "Imposto do Estado (\(tc.getFormattedValue(of: tc.taxaEstado, withCurrency: ""))%)"
		
		lbIofDescricao.text = "IOF (\(tc.getFormattedValue(of: tc.iof, withCurrency: ""))%)"
		
		lbDolar.text = tc.getFormattedValue(of: tc.valorCompra, withCurrency: "US$ ")
		lbImpostoEstado.text = tc.getFormattedValue(of: tc.valorTaxaEstado, withCurrency: "US$ ")
		lbIof.text = tc.getFormattedValue(of: tc.iof, withCurrency: "US$ ")
		
		let real = tc.calcular(usandoCartaoDeCredito: swCartaoCredito.isOn)
		
		lbReal.text = tc.getFormattedValue(of: real, withCurrency: "R$ ")
		
	}
	@IBAction func mudarIof(_ sender: Any) {
		calcularTaxas()
	}
	
}
