//
//  UIViewController+CalculadorTaxas.swift
//  ComprasUSA
//
//  Created by Paulo Gutemberg on 29/12/19.
//  Copyright © 2019 Paulo Gutemberg. All rights reserved.
//

import UIKit

extension UIViewController{
	var tc: CalculadorTaxas{
		return CalculadorTaxas.shared
	}
}
